package com.hrd._Mann_Socheat_Spring_Homework003.exception;

public class AuthorNotFoundException extends RuntimeException{
    public AuthorNotFoundException(Integer authorId){
        super("Author by id " + authorId+" not found");
    }
}
