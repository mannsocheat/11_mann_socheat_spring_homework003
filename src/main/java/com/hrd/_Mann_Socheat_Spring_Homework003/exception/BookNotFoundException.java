package com.hrd._Mann_Socheat_Spring_Homework003.exception;

public class BookNotFoundException extends RuntimeException {

    public BookNotFoundException(Integer book_id){
        super("Book by id " + book_id+" not found");
    }
}
