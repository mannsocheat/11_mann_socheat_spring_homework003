package com.hrd._Mann_Socheat_Spring_Homework003.exception;

public class GenderException extends RuntimeException{
    public GenderException(String gender){
        super(gender+" Incorrect "+"gender must be male and female");
    }
}
