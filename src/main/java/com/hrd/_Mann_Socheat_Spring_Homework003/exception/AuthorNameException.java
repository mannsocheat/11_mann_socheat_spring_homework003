package com.hrd._Mann_Socheat_Spring_Homework003.exception;

public class AuthorNameException extends RuntimeException{
    public AuthorNameException(String name){
        super(name + "is empty please input name");
    }
}
