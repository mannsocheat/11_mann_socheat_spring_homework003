package com.hrd._Mann_Socheat_Spring_Homework003.exception;

public class CategoryNameException extends RuntimeException{
    public CategoryNameException(String name){
        super(name+"name is empty please input name");
    }
}
