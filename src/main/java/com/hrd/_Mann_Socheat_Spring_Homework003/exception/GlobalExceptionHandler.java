package com.hrd._Mann_Socheat_Spring_Homework003.exception;

import org.springframework.http.*;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(AuthorNotFoundException.class)
    ProblemDetail handlerAuthorNotFoundException(AuthorNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle("not found exception");
        problemDetail.setType(URI.create("http:localhost:8080/api/v1/authorNotFound"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    ProblemDetail handlerCategoryNotFoundException(CategoryNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle("not found exception");
        problemDetail.setType(URI.create("http:localhost:8080/api/v1/CategoryNotFound"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }

    @ExceptionHandler(BookNotFoundException.class)
    ProblemDetail handlerBookNotFoundException(BookNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle("not found exception");
        problemDetail.setType(URI.create("http:localhost:8080/api/v1/BookNotFound"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp" , LocalDateTime.now());
        body.put("status", status.value());
        body.put("error", ex.getFieldError().getDefaultMessage());
        return new ResponseEntity<>(body, status);
    }

    @ExceptionHandler(GenderException.class)
    ProblemDetail handlerGenderException(GenderException e){
        ProblemDetail problemDetail= ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle("Insert incorrect");
        problemDetail.setType(URI.create("http:localhost:8080/api/v1/InsertIncorrect"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }

    @ExceptionHandler(AuthorNameException.class)
    ProblemDetail handlerAuthorNameException(AuthorNameException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle("name is empty");
        problemDetail.setType(URI.create("http:localhost:8080/api/v1/nameIsEmpty"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }

    @ExceptionHandler(CategoryNameException.class)
    ProblemDetail handlerCategoryException(CategoryNameException e){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle("name is empty");
        problemDetail.setType(URI.create("http:localhost:8080/api/v1/nameIsEmpty"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }

    @ExceptionHandler(TitleException.class)
    ProblemDetail handlerTitleException(TitleException e){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle("Title is empty");
        problemDetail.setType(URI.create("http:localhost:8080/api/v1/titleIsEmpty"));
        problemDetail.setProperty("timestamp", Instant.now());
        return problemDetail;
    }
}
