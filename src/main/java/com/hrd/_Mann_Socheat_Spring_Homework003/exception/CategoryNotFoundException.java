package com.hrd._Mann_Socheat_Spring_Homework003.exception;

public class CategoryNotFoundException extends RuntimeException{
    public CategoryNotFoundException(Integer categoryId){
        super("category by id " + categoryId+" not found");
    }
}
