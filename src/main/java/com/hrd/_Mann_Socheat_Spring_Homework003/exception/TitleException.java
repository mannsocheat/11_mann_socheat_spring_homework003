package com.hrd._Mann_Socheat_Spring_Homework003.exception;

public class TitleException extends RuntimeException{
    public TitleException(String name){
        super(name +"Title is empty ");
    }
}
