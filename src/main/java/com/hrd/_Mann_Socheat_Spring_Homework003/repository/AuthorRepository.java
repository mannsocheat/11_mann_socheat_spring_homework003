package com.hrd._Mann_Socheat_Spring_Homework003.repository;

import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Author;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.AuthorRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AuthorRepository {
    @Select("SELECT * FROM authors ORDER BY author_id")
    List<Author> getAllAuthor();

    @Select("SELECT * FROM authors WHERE author_id = #{authorId} ")
    Author getAuthorById(Integer authorId);
    @Select("INSERT INTO authors (author_name, gender) VALUES(#{request.author_name}, #{request.gender} )"+"RETURNING author_id ")
    Integer saveAuthor(@Param("request")AuthorRequest authorRequest);

    @Delete("DELETE FROM authors WHERE author_id=#{authorId}")
    boolean deleteAuthorById(Integer authorId);

    @Select("UPDATE authors "+
            "SET author_name=#{request.author_name}, " +
            "gender=#{request.gender} " +
            "WHERE author_id=#{authorId} " +
            "RETURNING author_id ")

    Integer updateAuthor(@Param("request") AuthorRequest authorRequest, Integer authorId);
}
