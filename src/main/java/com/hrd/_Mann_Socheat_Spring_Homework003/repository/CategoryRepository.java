package com.hrd._Mann_Socheat_Spring_Homework003.repository;

import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Category;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("SELECT c.category_id, c.category_name FROM book_details bd "+
            "INNER JOIN categories c ON c.category_id = bd.category_id "+
            "WHERE bd.book_id=#{bookId}; ")
    List<Category> getAllCategoryForBook(Integer bookId);
    @Select("SELECT * FROM categories ORDER BY category_id")
    List<Category> getAllCategory();
    @Select("SELECT * FROM categories WHERE category_id = #{categoryId} ")
    Category getCategoryById(Integer categoryId);
    @Select("INSERT INTO categories (category_name) VALUES(#{request.category_name} )"+"RETURNING category_id ")
    Integer saveCategory(@Param("request")CategoryRequest categoryRequest);
    @Delete("DELETE FROM categories WHERE category_id=#{categoryId}")
    boolean deleteCategoryById(Integer categoryId);

    @Select("UPDATE categories "+
            "SET category_name=#{request.category_name} " +
            "WHERE category_id=#{categoryId} " +
            "RETURNING category_id ")
    Integer updateCategory(@Param("request") CategoryRequest categoryRequest, Integer categoryId);

}
