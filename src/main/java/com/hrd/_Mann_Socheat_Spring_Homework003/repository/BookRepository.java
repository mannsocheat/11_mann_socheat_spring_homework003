package com.hrd._Mann_Socheat_Spring_Homework003.repository;

import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Book;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("SELECT * FROM books ORDER BY book_id")
    @Results(
            id="Mapping",
            value={
                    @Result(property = "timestamp", column = "published_date"),
                @Result(property = "book_id", column = "book_id") ,
                @Result(property = "author", column = "author_id",
                        one = @One(select = "com.hrd._Mann_Socheat_Spring_Homework003.repository.AuthorRepository.getAuthorById")

                ),
                    @Result(property = "category", column = "book_id",
                    many = @Many(select = "com.hrd._Mann_Socheat_Spring_Homework003.repository.CategoryRepository.getAllCategoryForBook")
                    )
    }
    )
    List<Book> getAllBook();

    @Select("SELECT * FROM books  WHERE book_id=#{bookId} ")
    @ResultMap("Mapping")
    Book getBookById(Integer bookId);

    @Select("INSERT INTO books (title, published_date, author_id) " +
            "VALUES(#{request.title}, #{request.published_date}, #{request.authorId}) "+
            "RETURNING book_id")
    Integer saveBook(@Param("request")BookRequest bookRequest);

    @Select("INSERT INTO book_details (book_id, category_id)" +
            "VALUES(#{book_id}, #{category_id})")
    Integer saveCategoryById(Integer book_id , Integer category_id);

    @Delete("DELETE FROM books WHERE book_id=#{bookId}")
    boolean deleteBookById(Integer bookId);


    @Delete("DELETE FROM book_details WHERE book_id=#{book_id} ")
    boolean delete(Integer bookId);
    @Select("UPDATE books " +
            "SET title=#{request.title}, " +
            "published_date=#{request.published_date}," +
            "author_id=#{request.authorId} " +
            "WHERE book_id=#{book_id} " +
            "RETURNING book_id")
    Integer updateInvoiceById(@Param("request") BookRequest bookRequest, Integer book_id);

}
