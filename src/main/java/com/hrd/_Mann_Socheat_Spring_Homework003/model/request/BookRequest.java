package com.hrd._Mann_Socheat_Spring_Homework003.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookRequest {
    private String title;
    private Timestamp published_date;
    private Integer authorId;
    private List<Integer> book;
}
