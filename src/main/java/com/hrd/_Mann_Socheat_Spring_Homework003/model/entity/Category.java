package com.hrd._Mann_Socheat_Spring_Homework003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class Category {
    private Integer category_id;
    private String category_name;
}
