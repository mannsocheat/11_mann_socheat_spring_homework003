package com.hrd._Mann_Socheat_Spring_Homework003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Author {
    private Integer author_id;
    private  String author_name;
    private String gender;
}
