package com.hrd._Mann_Socheat_Spring_Homework003.service;

import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Book;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.BookRequest;

import java.util.List;

public interface BookService {
List<Book> getAllBook();
Book getBookById(Integer bookId);
Integer addNewBook(BookRequest bookRequest);
boolean deleteBookById(Integer bookId);
Integer updateBookById(BookRequest bookRequest, Integer bookId);
}
