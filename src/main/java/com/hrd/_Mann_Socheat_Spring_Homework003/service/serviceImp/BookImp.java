package com.hrd._Mann_Socheat_Spring_Homework003.service.serviceImp;

import com.hrd._Mann_Socheat_Spring_Homework003.exception.AuthorNotFoundException;
import com.hrd._Mann_Socheat_Spring_Homework003.exception.CategoryNotFoundException;
import com.hrd._Mann_Socheat_Spring_Homework003.exception.TitleException;
import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Book;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.BookRequest;
import com.hrd._Mann_Socheat_Spring_Homework003.repository.AuthorRepository;
import com.hrd._Mann_Socheat_Spring_Homework003.repository.BookRepository;
import com.hrd._Mann_Socheat_Spring_Homework003.repository.CategoryRepository;
import com.hrd._Mann_Socheat_Spring_Homework003.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookImp implements BookService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final CategoryRepository categoryRepository;

    public BookImp(BookRepository bookRepository, AuthorRepository authorRepository, CategoryRepository categoryRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Book> getAllBook() {
        return bookRepository.getAllBook();
    }

    @Override
    public Book getBookById(Integer bookId) {
        return bookRepository.getBookById(bookId);
    }

    @Override
    public Integer addNewBook(BookRequest bookRequest) {
        if (bookRequest.getTitle().isBlank()){
            throw new TitleException(bookRequest.getTitle());
        }
        if (!(authorRepository.getAuthorById(bookRequest.getAuthorId()) !=null)){
            throw new AuthorNotFoundException(bookRequest.getAuthorId());
        }else {
            for (Integer categoryId:bookRequest.getBook()){
                if (categoryRepository.getCategoryById(categoryId) ==null){
                    throw new CategoryNotFoundException(categoryId);
                }else {
                    Integer storeBookId = bookRepository.saveBook(bookRequest);
                    bookRepository.saveCategoryById(storeBookId,categoryId);
                    return storeBookId;
                }

            }
            return null;
        }



    }

    @Override
    public boolean deleteBookById(Integer bookId) {
        return bookRepository.deleteBookById(bookId);
    }

    @Override
    public Integer updateBookById(BookRequest bookRequest, Integer bookId) {
        if (bookRequest.getTitle().isBlank()){
            throw new TitleException(bookRequest.getTitle());
        }
        if (authorRepository.getAuthorById(bookRequest.getAuthorId()) ==null){
            throw new AuthorNotFoundException(bookRequest.getAuthorId());
        }
        else {

            for (Integer update: bookRequest.getBook()){
                if (categoryRepository.getCategoryById(update) ==null){
                    throw new CategoryNotFoundException(update);
                }else {
                    Integer storeBookId = bookRepository.updateInvoiceById(bookRequest,bookId);
                    bookRepository.delete(bookId);
                    bookRepository.saveCategoryById(storeBookId,update);
                    return storeBookId;
                }

            }
        }

        return null;
    }
}
