package com.hrd._Mann_Socheat_Spring_Homework003.service.serviceImp;

import com.hrd._Mann_Socheat_Spring_Homework003.exception.CategoryNameException;
import com.hrd._Mann_Socheat_Spring_Homework003.exception.CategoryNotFoundException;
import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Category;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.CategoryRequest;
import com.hrd._Mann_Socheat_Spring_Homework003.repository.CategoryRepository;
import com.hrd._Mann_Socheat_Spring_Homework003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryImp implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public Category getCategoryById(Integer categoryId) {
        return categoryRepository.getCategoryById(categoryId);
    }
    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest) {
        if (categoryRequest.getCategory_name().isBlank()){
            throw new CategoryNameException(categoryRequest.getCategory_name());
        }
        return categoryRepository.saveCategory(categoryRequest);
    }

    @Override
    public boolean deleteCategory(Integer categoryId) {
        return categoryRepository.deleteCategoryById(categoryId);
    }

    @Override
    public Integer updateCategoryById(CategoryRequest categoryRequest, Integer categoryId) {
        if (categoryRequest.getCategory_name().isBlank()){
            throw new CategoryNameException(categoryRequest.getCategory_name());
        }else {
            if (categoryRepository.updateCategory(categoryRequest,categoryId) !=null){
                return categoryRepository.updateCategory(categoryRequest,categoryId);
            }else {
                throw new CategoryNotFoundException(categoryId);
            }
        }

    }
}
