package com.hrd._Mann_Socheat_Spring_Homework003.service.serviceImp;

import com.hrd._Mann_Socheat_Spring_Homework003.exception.AuthorNameException;
import com.hrd._Mann_Socheat_Spring_Homework003.exception.AuthorNotFoundException;
import com.hrd._Mann_Socheat_Spring_Homework003.exception.GenderException;
import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Author;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.AuthorRequest;
import com.hrd._Mann_Socheat_Spring_Homework003.repository.AuthorRepository;
import com.hrd._Mann_Socheat_Spring_Homework003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorRepository.getAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer authorId) {
        return authorRepository.getAuthorById(authorId);
    }

    @Override
    public Integer addNewAuthor(AuthorRequest authorRequest) {
        if (authorRequest.getAuthor_name().isBlank()){
            throw new AuthorNameException(authorRequest.getAuthor_name());
        }
        if( !(authorRequest.getGender().equals("male") || authorRequest.getGender().equals("female"))){
            throw new GenderException(authorRequest.getGender());
        }
        return authorRepository.saveAuthor(authorRequest);
    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
        return authorRepository.deleteAuthorById(authorId);
    }

    @Override
    public Integer updateAuthorById(AuthorRequest authorRequest, Integer authorId) {
        if (authorRequest.getAuthor_name().isBlank()){
            throw new AuthorNameException(authorRequest.getAuthor_name());
        }
        if( !(authorRequest.getGender().equals("male") || authorRequest.getGender().equals("female"))){
            throw new GenderException(authorRequest.getGender());
        }
        else {
            if (authorRepository.updateAuthor(authorRequest,authorId) !=null){
                return authorRepository.updateAuthor(authorRequest,authorId);
            }else {
                throw new AuthorNotFoundException(authorId);
            }
        }

    }
}
