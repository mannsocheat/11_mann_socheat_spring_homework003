package com.hrd._Mann_Socheat_Spring_Homework003.service;

import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Category;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {
List<Category> getAllCategory();
Category getCategoryById(Integer categoryId);
Integer addNewCategory(CategoryRequest categoryRequest);
boolean deleteCategory(Integer categoryId);
Integer updateCategoryById(CategoryRequest categoryRequest, Integer categoryId);
}
