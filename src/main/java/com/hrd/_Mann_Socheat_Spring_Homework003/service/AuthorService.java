package com.hrd._Mann_Socheat_Spring_Homework003.service;

import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Author;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();
    Author getAuthorById(Integer authorId);

    Integer addNewAuthor(AuthorRequest authorRequest);
    boolean deleteAuthorById(Integer authorId);

    Integer updateAuthorById(AuthorRequest authorRequest, Integer authorId);
}
