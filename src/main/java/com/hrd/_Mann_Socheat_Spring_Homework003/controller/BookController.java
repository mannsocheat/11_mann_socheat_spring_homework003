package com.hrd._Mann_Socheat_Spring_Homework003.controller;

import com.hrd._Mann_Socheat_Spring_Homework003.exception.BookNotFoundException;
import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Book;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.BookRequest;
import com.hrd._Mann_Socheat_Spring_Homework003.model.response.BookResponse;
import com.hrd._Mann_Socheat_Spring_Homework003.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("allBook")
    @Operation(summary = "get all book")
    public ResponseEntity<BookResponse<List<Book>>> getAllBook(){
        BookResponse<List<Book>> response = BookResponse.<List<Book>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully fetched books")
                .payload(bookService.getAllBook())
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("getBookById{book_id}")
    public ResponseEntity<BookResponse<Book>> getBookById(@PathVariable("book_id") Integer bookId){

        if (bookService.getBookById(bookId) != null){
            BookResponse<Book> response = BookResponse.<Book>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully fetched Book")
                    .payload(bookService.getBookById(bookId))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new BookNotFoundException(bookId);
        }
    }

    @PostMapping("addBook")
    @Operation(summary = "add new book")
    public ResponseEntity<BookResponse<Book>> addBook(@RequestBody BookRequest bookRequest){
        BookResponse<Book> response=null;
        Integer storeBookId = bookService.addNewBook(bookRequest);
        if (storeBookId !=null){
             response = BookResponse.<Book>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully added Book")
                    .payload(bookService.getBookById(storeBookId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @DeleteMapping("deleteBook{book_id}")
    public ResponseEntity<BookResponse<String>> deleteBookById(@PathVariable("book_id") Integer bookId){
        if (bookService.deleteBookById(bookId) == true){
            BookResponse<String> response = BookResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully deleted book")
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new BookNotFoundException(bookId);
        }
    }

    @PutMapping("updateBook{book_id}")
    @Operation(summary = "Update book by id")
      public ResponseEntity<BookResponse<Book>> updateBookById(@RequestBody BookRequest bookRequest, @PathVariable("book_id") Integer bookId){
         Integer storeIdUpdate = bookService.updateBookById(bookRequest,bookId);
          if (storeIdUpdate !=null){
              BookResponse<Book> response = BookResponse.<Book>builder()
                      .timestamp(new Timestamp(System.currentTimeMillis()))
                      .status(200)
                      .message("Successfully updated Book")
                      .payload(bookService.getBookById(storeIdUpdate))
                      .build();
              return ResponseEntity.ok(response);
          }else {
              throw new BookNotFoundException(bookId);
          }

}
}
