package com.hrd._Mann_Socheat_Spring_Homework003.controller;

import com.hrd._Mann_Socheat_Spring_Homework003.exception.CategoryNotFoundException;
import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Category;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.CategoryRequest;
import com.hrd._Mann_Socheat_Spring_Homework003.model.response.CategoryResponse;
import com.hrd._Mann_Socheat_Spring_Homework003.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/getAllCategory")
    @Operation(summary = "get all category")
    public ResponseEntity<CategoryResponse<List<Category>>> getAllCategory(){
        CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully fetched categories")
                .payload(categoryService.getAllCategory())
                .build();

        return ResponseEntity.ok(response);

    }

    @GetMapping("getCategoryById{category_id}")
    @Operation(summary = "get category by id")
    public ResponseEntity<CategoryResponse<Category>> getCategoryById(@PathVariable("category_id") Integer categoryId){
        if (categoryService.getCategoryById(categoryId) !=null){
            CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully fetched category")
                    .payload(categoryService.getCategoryById(categoryId))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new CategoryNotFoundException(categoryId);
        }
    }

    @PostMapping("/input-Category")
    @Operation(summary = "Insert category")
    public ResponseEntity<CategoryResponse<Category>> addNewCategory(@RequestBody CategoryRequest categoryRequest ){
        Integer storeCategoryId = categoryService.addNewCategory(categoryRequest);
        if (storeCategoryId !=null){
            CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully added categories")
                    .payload(categoryService.getCategoryById(storeCategoryId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
    @DeleteMapping("/delete{category_id}")
    @Operation(summary = "delete category by id")
    public ResponseEntity<CategoryResponse<Category>> deleteCategoryById(@PathVariable("category_id") Integer categoryId){
       if (categoryService.deleteCategory(categoryId) ==true){
           CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                   .timestamp(new Timestamp(System.currentTimeMillis()))
                   .status(200)
                   .message("Successfully deleted category")
                   .build();
           return ResponseEntity.ok(response);
       }else {
           throw new CategoryNotFoundException(categoryId);
       }
    }

    @PutMapping("updateCategory{category_id}")
    @Operation(summary = "update category by id")
    public ResponseEntity<CategoryResponse<Category>> updateCategoryById(@RequestBody CategoryRequest categoryRequest, @PathVariable("category_id") Integer categoryId){
        if (categoryService.updateCategoryById(categoryRequest, categoryId) !=null){
            CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully updated category")
                    .payload(categoryService.getCategoryById(categoryId))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new CategoryNotFoundException(categoryId);
        }
    }
}
