package com.hrd._Mann_Socheat_Spring_Homework003.controller;

import com.hrd._Mann_Socheat_Spring_Homework003.exception.AuthorNotFoundException;
import com.hrd._Mann_Socheat_Spring_Homework003.model.entity.Author;
import com.hrd._Mann_Socheat_Spring_Homework003.model.request.AuthorRequest;
import com.hrd._Mann_Socheat_Spring_Homework003.model.response.AuthorResponse;
import com.hrd._Mann_Socheat_Spring_Homework003.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("all")
    @Operation(summary = "get all author")
    public ResponseEntity<AuthorResponse<List<Author>>>  getAllAuthor(){
        AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                .timestampl(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully fetched authors")
                .payload(authorService.getAllAuthor())
                .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/searchById{author_id}")
    @Operation(summary = "Search author by id")
    public ResponseEntity<AuthorResponse<Author>> getAuthorById(@PathVariable("author_id") Integer authorId){
       if (authorService.getAuthorById(authorId) !=null){
           AuthorResponse<Author> response =AuthorResponse.<Author>builder()
                   .timestampl(new Timestamp(System.currentTimeMillis()))
                   .status(200)
                   .message("Successfully fetched authors")
                   .payload(authorService.getAuthorById(authorId))
                   .build();
           return ResponseEntity.ok(response);
       }else throw new AuthorNotFoundException(authorId);
    }

    @PostMapping("/input-author")
    @Operation(summary = "input author")
    public ResponseEntity<AuthorResponse<Author>> addNewAuthor( @RequestBody AuthorRequest authorRequest){
       Integer storeId = authorService.addNewAuthor(authorRequest);
        if ( storeId !=null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .timestampl(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully added author")
                    .payload(authorService.getAuthorById(storeId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
    @DeleteMapping("/{author_id}")
    @Operation(summary = "delete author by id")
    public ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable("author_id") Integer authorId){

        if (authorService.deleteAuthorById(authorId) == true){
            AuthorResponse<String> response = AuthorResponse.<String>builder()
                    .timestampl(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully deleted author")
                    .build();
            return ResponseEntity.ok(response);
        }else throw new AuthorNotFoundException(authorId);

    }
    @PutMapping("updateAuthor{author_id}")
    @Operation(summary = "update author by id")
    public ResponseEntity<AuthorResponse<Author>> updateAuthorById(@RequestBody AuthorRequest authorRequest, @PathVariable("author_id") Integer authorId){
        if (authorService.updateAuthorById(authorRequest,authorId) !=null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .timestampl(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully updated author")
                    .payload(authorService.getAuthorById(authorId))
                    .build();
            return ResponseEntity.ok(response);
        }else throw new AuthorNotFoundException(authorId);
    }




}
